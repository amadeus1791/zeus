<?php 

class Portafolio_model extends CI_Model {

    public function __construct(){
            parent::__construct();
    }

    
	public function infoPorAlias($alias){
        $query = $this->db->get_where("cliente",array("cli_alias" => $alias));
        if($query->num_rows()==0) return null;
        else return $query->first_row();
    }
}