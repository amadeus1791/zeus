<?php
    class Contacto_model extends CI_Model{

        function __construct(){
            parent::__construct();
        }

        public function listar($estado,$pry_id){

            $sql = 'SELECT * FROM lote where  pry_id='.$pry_id.' and lte_estado='.$estado.' ORDER BY lte_orden ASC';

            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function listarpornombres($nombre){

            $sql = "SELECT count(lte_imagen) as nombres FROM lote where lte_imagen like '%".$nombre."%'";

            $query = $this->db->query($sql);
            
            
            if($query->num_rows()>0){
                return $query->first_row();
            }else{
                return FALSE;
            }
        }


        public function guardar($param){

            return $this->db->insert('contacto', $param);
        }
}
?>
