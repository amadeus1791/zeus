<?php

class Newsletter_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function suscripcion($data){
        $this->db->insert('newsletter', $data);
        return $this->db->insert_id();
    }


    public function listar($ini)
    {
        $sql = 'select * from newsletter order by news_id ASC LIMIT '. $ini.',50';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function total()
    {
        $sql = 'select count(*) as total from newsletter';
        $query = $this->db->query($sql);

        $row = $query->first_row();

        return $row->total;

    }

    public function descargaremail($id)
    {
        $sql = "select news_id  as ID, 
                news_email as 'Email' 
                from newsletter";
        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

    }



}
