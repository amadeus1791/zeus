<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function index()
    {

        $this->versesion();


        $this->load->view('admin/header');
        $this->load->view('admin/home');
        $this->load->view('admin/footer');

    }


    private function versesion(){

        if(!$this->session->has_userdata('admin')){
            redirect('admin/login');
            exit();
        }
    }
    public function login()
    {
        $this->load->view('admin/login');
    }


    public function newsletter(){
        $this->versesion();


        $this->load->view('admin/header');
        $this->load->view('admin/newsletter');
        $this->load->view('admin/footer');
    }


}
