<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Newsletter_model','newsletter');
    }

    public function suscripcion(){

        $email = $this->input->post("email");


        $data = array(
            "news_email" => $email
        );

        $suscripcion = $this->newsletter->suscripcion($data);

        $res["res"] = $suscripcion;
        $res["res"] = "ok";

        echo json_encode($res);
    }


    public function listar(){

        if(!$this->session->has_userdata('admin')){
            exit();
        }

        $pg = $this->input->get("pagina");
        $cant = 50;
        $ini = ($pg -1)* $cant;

        $lista = $this->newsletter->listar($ini);

        echo json_encode($lista);

    }

    public function total(){

        if(!$this->session->has_userdata('admin')){
            exit();
        }

        $res['total'] = $this->newsletter->total();

        echo json_encode($res);

    }
    function descargaremail($id){
        if(!$this->session->has_userdata('admin')){
            exit();
        }
        if($id==0) $id = '%';
        $lista = $this->newsletter->descargaremail($id);

        echo json_encode($lista);
    }


}
