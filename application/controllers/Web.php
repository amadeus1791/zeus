<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Portafolio_model','portafolio');
	}

	public function index()
	{
		$header["titulo"] = "Home";
		$header['page'] = 'home';

		$this->load->view('header',$header);
		$this->load->view('home');
		$this->load->view('footer');
	}


	public function portafolio()
	{
		$header["titulo"] = "Portafolio";
		$header['page'] = 'portafolio';

		$this->load->view('header',$header);
		$this->load->view('portafolio');
		$this->load->view('footer');
	}

	public function nosotros()
	{
		$header["titulo"] = "Nosotros";
		$header['page'] = 'nosotros';

		$this->load->view('header',$header);
		$this->load->view('nosotros');
		$this->load->view('footer');
	}

	public function contacto()
	{
		$header["titulo"] = "Contacto";
		$header['page'] = 'contacto';

		$this->load->view('header',$header);
		$this->load->view('contacto');
		$this->load->view('footer');
	}


	public function infoportafolio($alias){
		

		$info = $this->portafolio->infoPorAlias($alias);

		$header["titulo"] = $info->cli_nombre." | Portafolio ";
		$header['page'] = 'infoportafolio';


		$data = array(
			"info" => $info
		);

		$this->load->view('header',$header);
		$this->load->view('infoportafolio',$data);
		$this->load->view('footer');	
	}



}
