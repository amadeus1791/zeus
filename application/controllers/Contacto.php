<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('Contacto_model','contacto');
     $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'martin@zeus.pe',
            'smtp_pass' => 'Salirrosas123',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
      );
      $this->load->library('email', $config);
      $this->email->initialize($config);
  }


   public function enviar(){

       $param['cto_nombres'] = $this->input->post('nombres');
       $param['cto_email'] = $this->input->post('email');
       $param['cto_ruc'] = $this->input->post('ruc');
       $param['cto_telefono'] = $this->input->post('telefono');
       $param['cto_donde'] = $this->input->post('dondeCoti');
       $param['cto_puntosventa'] = $this->input->post('ptosVenta');
       $param['cto_cantidad'] = $this->input->post('cantidad');
       $param['cto_visitas'] = $this->input->post('visitas');
       $param['cto_cotizacion'] = $this->input->post('cotizacion');


       $guardado = $this->contacto->guardar($param);
      
       $envio = false;

       if($guardado){ 
          $envio = $this->correoContacto($param);  
       }
       if($envio != false){

           $res["res"] = "ok";
           $res["envio"] = $envio;
           echo json_encode($res);
       }
       else{
           $res["res"] = "no";
           $res["envio"] = $envio;
           echo json_encode($res);
       }

    }

    public function correoContacto($param){


       $this->email->set_newline("\r\n");
     
       $this->email->from('martin@zeus.pe', "Zeus");
       $this->email->to($param['cto_email'], $param['cto_nombres']);

       $this->email->subject("Zeus - Cotización");

      $row = array(
        'nombres' => $param['cto_nombres'],
        'donde'=> $param['cto_donde'],
        'venta'=>$param['cto_puntosventa'],
        'cantidad'=>$param['cto_cantidad'],
        'visitas'=>$param['cto_visitas'],
        'cotizacion' =>$param['cto_cotizacion']
      );
        
      $body =  $this->load->view('mails/respuesta.php',$row,TRUE);
      // var_dump($body);
      $this->email->message($body);
      
      if( $this->email->send()){
          $envio = true;
      }else{
          $envio = false;
      }

      return $envio;


  }

  

}
