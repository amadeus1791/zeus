<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/infoportafolio.css">


<div class="container-fluid" style="background-color: #f8f8f8;">
    <div class="container banner__clientes">
        <img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>.jpg" alt="">
    </div>

</div>


<div class="container-fluid <?php echo $info->cli_alias; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 left-content">
				<p class="text__important">
					<?php echo $info->cli_texto; ?>
				</p>
			</div>
			<div class="col-lg-7 right-content">
				<div class="cliente__item">
					<h3 class="info__title">Situación:</h3>
					<?php echo $info->cli_situacion; ?>
				</div>


				<?php if ($info->cli_video == 1) { ?>
					<a href="<?php echo $info->cli_videolink; ?>" class="video__link popup-youtube" style="background-image:url(<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-video.jpg">
						
					</a>
                <?php } elseif ($info->cli_video == 3) {?>

				<?php } else { ?>
					<img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-primary-banner.jpg" class="primary__banner" alt="">
				<?php }  ?>


				
				<div class="cliente__item">
					<h3 class="info__title">Solución:</h3>
					<?php echo $info->cli_solucion; ?>
				</div>	


				<?php if ($info->cli_second == 1) { ?>
					<img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-second-banner.png" class="second__banner" alt="">
                <?php } elseif ($info->cli_second == 3) {?>
                <div class="cliente__item pt-0 pb-0" >
                    <h3 class="info__title mt-3" style="text-decoration: underline">Caso: “El guantazo”</h3>
                    <a href="https://www.youtube.com/watch?v=7u5fz-cqpvE" class="video__link video__link--b popup-youtube" style="background-image:url(<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-caso1.jpg">

                    </a>
                    <h3 class="info__title mt-3"  style="text-decoration: underline">Caso: “Sílbale a tu madre”</h3>
                    <a href="https://www.youtube.com/watch?v=ZnD64nqYQ1U" class="video__link video__link--b popup-youtube" style="background-image:url(<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-caso2.jpg">

                    </a>
                </div>
                <?php } else { ?>
					<img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-second-banner.png" class="second__banner--small" alt="">
				<?php }  ?>
				
				


				<div class="cliente__item">
					<h3 class="info__title">Resultado:</h3>
					<?php echo $info->cli_resultado; ?>
				</div>

				<?php if ($info->cli_second == 0) { ?>
					<img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-third-banner.jpg" class="third__banner" alt="">
                <?php } elseif ($info->cli_second == 3) {?>
                    <img src="<?php echo base_url(); ?>assets/img/clientes/<?php echo $info->cli_alias; ?>-third-banner.jpg" class="third__banner" alt="">
                <?php } ?>

			</div>
		</div>
	</div>
	
	
</div>