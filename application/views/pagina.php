<!doctype html>
<html lang="es">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/slick/slick.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/slick/slick-theme.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/owl-carousel/owl.carousel.min.css">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">

  <script>
    var path = '<?php echo base_url(); ?>';
  </script>
  <title>Zeus.pe | <?php echo $titulo; ?></title>
  
</head>
<body>

<div class="modal fade" id="cotizacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="container-fluid">
                <div class="row justify-content-end mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6 d-none d-lg-block">
                        <img width="100%" src="<?php echo base_url() ;?>assets/img/undraw_business_deal_cpi9.svg" alt="">
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <form action="<?php echo base_url()?>contacto/enviar" method="post">
                            <input type="hidden" name="dondeCoti">
                            <input type="hidden" name="ptosVenta">
                            <input type="hidden" name="cantidad">
                            <input type="hidden" name="visitas">
                            <input type="hidden" name="cotizacion">

                            <div class="form-group">
                                <label for=""><i class="fas fa-user"></i>Nombre y Apellido</label>
                                <input type="text" name="nombres" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-envelope"></i>Correo Electrónico</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-newspaper"></i>RUC</label>
                                <input type="number" name="ruc" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-phone"></i>Teléfono</label>
                                <input type="text" name="telefono" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-zeus">¡Hagamos un negocio!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="enviado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="container-fluid">

                <div class="row text-center">
                    <div class="col-lg-8 offset-lg-2">
                        <img width="100%" src="<?php echo base_url() ?>assets/img/enviado.svg" alt="">
                    </div>
                    <div class="col-lg-12 text-center">
                        <strong>Enviado correctamente</strong>
                        <button class="btn-zeus" data-dismiss="modal" aria-label="Close">¡Aceptar!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<nav class="menu">
    <div class="container navbar">
        <a href="<?php echo base_url(); ?>" class="menu__brand">
          <img src="<?php echo base_url() ?>assets/img/logo.svg" alt="">
        </a>
        <div class="menu__button">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="close__button">
          <i class="fas fa-times"></i>
        </div>
        <ul class="menu__box">
            <li class="menu__item logo-resp">
                <img src="<?php echo base_url() ?>assets/img/logo-blanco.svg" alt="">
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#cotiza">Cotiza</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#mercaderismo">Mercaderismo</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#beneficios">Beneficios</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#" data-toggle="modal" data-target="#cotizacion">Contacto</a>
            </li>
            <li class="menu__item redes">
                <a class="menu__link" href="#"><span>Síguenos en:</span> <i class="fab fa-facebook"></i></a>
            </li>
        </ul>
    </div>
</nav>

<section class="banner">


    <div class="slick-home">
        <div class="no-back">
            <div class="row align-items-center">

                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img src="<?php echo base_url() ?>assets/img/botellas-banner.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row align-items-center">

                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img src="<?php echo base_url() ?>assets/img/b1.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row align-items-center">
                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img class="d-none d-md-block" src="<?php echo base_url() ?>assets/img/b2-v1.png" alt="">
                    <img class="d-block d-md-none" src="<?php echo base_url() ?>assets/img/b2-v3.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>

                </div>
            </div>
        </div>
    </div>


</section>

<section class="incrementar" id="incrementar">

    <div class="incrementar__left">
        <img src="<?php echo base_url() ?>assets/img/laptop.png" alt="" class="laptop">
    </div>
    <div class="incrementar__right">
        <h4>
            Empieza a Incrementar tus ventas
        </h4>
        <p>
            Ponemos tu producto al alcance del cliente, mejorando su visibilidad y disposición. Además, te brindamos
            herramientas digitales que faciliten tu gestión y seguimiento a tiempo real.
        </p>
        <div class="numbers">
            <div class="numbers__item">
                <span>+25%</span>
                <p>ventas</p>
            </div>
            <div class="numbers__item">
                <span>+40%</span>
                <p>Alcance</p>
            </div>
            <div class="numbers__item">
                <span>-30%</span>
                <p>Costo</p>
            </div>
        </div>
    </div>
</section>

<section class="cotizador" id="cotiza">
    <div class="zeus-title">
        Un servicio a tu alcance
    </div>
    <p class="zeus-subtitle">
        Ofrecemos el servicio más flexible. Calcula el costo de nuestro servicio de acuerdo a tu necesidad.
    </p>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">

                <div class="form-group">
                    <label for="">¿Dónde ventes tus productos?</label>

                    <div class="block-select">
                        <img src="<?php echo base_url() ?>assets/img/arrow-down.svg" alt="">
                        <select name="donde" id="">
                            <option value="supermercado">Supermercado</option>
                            <option value="tienda">Tienda</option>

                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <label for="">¿Cuántos son tus puntos de venta?</label>
                    <div class="handle-counter" id="venta">
                        <button class="counter-minus">-</button>
                        <input type="text" name="punto" value="1" pattern="\d*"/>
                        <button class="counter-plus">+</button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">¿Cuántos productos ofreces (SKU)?</label>
                    <div class="handle-counter" id="tipo">
                        <button class="counter-minus">-</button>
                        <input type="text" name="sku" value="1" pattern="\d*"/>
                        <button class="counter-plus">+</button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Selecciona la cantidad visitas:</label>

                    <div class="block-select">
                        <img src="<?php echo base_url() ?>assets/img/arrow-down.svg" alt="">
                        <select name="visitas" id="">
                            <option value="4">1 visita por semana (Recomendado)</option>
                            <option value="8">2 visitas por semana</option>
                            <option value="12">3 visitas por semana</option>
                            <option value="16">4 visitas por semana</option>
                            <option value="20">5 visitas por semana</option>
                            <option value="24">6 visitas por semana</option>
                            <option value="28">7 visitas por semana</option>
                            <option value="1">1 visita al Mes</option>
                            <option value="2">2 visitas al Mes</option>
                            <option value="3">3 visitas al Mes</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-6">
                <div class="result">
                    <span>Tu pago mensual sería</span>

                    <div class="price">
                        <i>s/</i>
                        <div class="num" id="num">
                            <strong>00</strong> <span>*</span>
                            <input type="hidden" name="coti"/>
                        </div>
                        <p>Mercaderismo + seguimiento virtual</p>
                        <strong>*No incluye IGV</strong>
                    </div>
                    <button class="btn-zeus" data-toggle="modal" data-target="#cotizacion">¡COTIZA AQUÍ!</button>
                </div>

                <div class="duda">¿Tienes dudas? Escríbenos</div>
            </div>
        </div>
    </div>
</section>


<section class="diferencia" id="beneficios">
    <div class="container">
        <div class="zeus-title">Un servicio integral</div>
        <p class="zeus-subtitle">
            Con Zeus podrás tener mayor control y visibilidad de la exposición de tus productos</p>

        <div class="row diferencia-carousel">
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/clock.svg" alt="">
                    <div>
                        <strong>Información real y precisa</strong>
                        <p>
                            Monitoreamos la cantidad y posición de tus productos en góndola, almacén y fechas de
                            vencimiento para brindarte información a tiempo real.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/stock.svg" alt="">
                    <div>
                        <strong>Niveles de Stock Optimizados</strong>
                        <p>
                            Optimizamos los niveles de stock en base a tu prioridad de ventas para que pedas tener más
                            cantidad de productos en góndola y menos en almacén.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/eficientes.svg" alt="">
                    <div>
                        <strong>Reposiciones Eficientes en Tienda</strong>
                        <p>
                            Nos encargamos de tus productos desde la salida de tu almacén hasta la entrega. Con nuestra
                            opción de envíos (moto, auto o camión) automatizamos tus entregas para que sean más
                            puntuales y eficientes.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/preventivo.svg" alt="">
                    <div>
                        <strong>Control preventivo de Perdidas</strong>
                        <p>
                            En nuestros reportes periódicos , te informamos acerca del estado y vencimiento de tus
                            productos en stock para que puedas tomar medidas preventivas y oportunas.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="clientes">
    <div class="container">
        <div class="zeus-title">Nuestra Experiencia</div>
        <p class="zeus-subtitle">Trabajamos para hacer del mercaderismo un servicio más eficiente y sostenible </p>



        <div class="wraper-clientes">

            <div class="experiencia">
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/productos.svg" alt="">
                    <strong>+800.000</strong>
                    <p>Productos posicionados en el punto de venta</p>
                </div>
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/location.svg" alt="">
                    <strong>+500</strong>
                    <p>Puntos de venta visitados mensualmente</p>
                </div>
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/time.svg" alt="">
                    <strong>+20,000</strong>
                    <p>Horas de ejecución impecable al año</p>
                </div>
            </div>

            <div class="testimonios">
                <div class="testimonio show" id="nutrishake">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/nutrishake-back.jpg)">

                    </div>
                    <div class="test-info">
                        <p>“Gracias a los servicios de Zeus logramos mejorar nuestras ventas en un 20% y reducir nuestros
                            costos en 25%. Además de reducir el tiempo de 4 a 1 día para abastecer y posicionar nuestros
                            productos en las tiendas de autoservicios(Tambo 200 y Listo 100).”</p>

                        <span>Daniel Nuñez</span>
                        <span>CEO & Founder </span>
                        <strong>Nutrishake</strong>
                    </div>
                </div>
                <div class="testimonio" id="vidria">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/vidria-back.jpg)">
                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo en supermercados de Zeus, la frecuencia de pedidos nuevos paso
                            de 1 a 2 por mes. Además que tenemos más espacio en góndola.”</p>

                        <span>Marco Riva</span>
                        <span>CEO</span>
                        <strong>Vidria</strong>
                    </div>
                </div>
                <div class="testimonio" id="ecohouse">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/ecohouse-back.jpg)">

                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo de Zeus aumentamos nuestra presencia en góndola en un 30% lo que
                            generó un aumento en ventas del 15%.”</p>

                        <span>Gina Scamarone</span>
                        <span>Gerente Comercial</span>
                        <strong>Ecohouse</strong>
                    </div>
                </div>
                <div class="testimonio" id="prots">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/prots-back.jpg)">
                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo de Zeus en Tottus, tenemos un mejor monitoreo de nuestros
                            productos por vencer en cada tienda, lo cual nos permite tomar medidas correctivas y reducir nuestra
                            merma en un 30%.”</p>

                        <span>Billy Jimenez</span>
                        <span>Jefe de Producción</span>
                        <strong>Prots</strong>
                    </div>
                </div>

            </div>

            <div class="row buttons-clientes">
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item active" data-cliente="nutrishake">
                        <img src="<?php echo base_url() ?>assets/img/nutrishake.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="vidria">
                        <img src="<?php echo base_url() ?>assets/img/vidria-logo.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="ecohouse">
                        <img src="<?php echo base_url() ?>assets/img/house.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="prots">
                        <img src="<?php echo base_url() ?>assets/img/prots.svg" alt="">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<footer>
    <div class="container">
       <div class="row">
           <div class="col-lg-3">
               <img width="150px" src="<?php echo base_url()?>assets/img/logo-blanco.svg" alt="">
           </div>
           <div class="col-lg-6">
               <strong>¿Quiéres saber más?</strong>
               <p>Te invitamos a contactarnos a través de los siguientes canales de comunicación, te atenderemos en la brevedad posible.</p>
           </div>
           <div class="col-lg-3">
              <div class="sociales">
                  <a href=""><i class="fas fa-envelope"></i></a>
                  <a href=""><i class="fab fa-whatsapp"></i></a>
                  <a href=""><i class="fab fa-facebook-messenger"></i></a>
              </div>
           </div>
       </div>
       <div class="row copy">
         <div class="col-lg-6 offset-lg-3 text-center">
           © Copyright 2012 - 2019   |   All Rights Reserved  
         </div>
         <div class="col-lg-3 text-md-right">
           Síguenos en <a href=""><i class="fab fa-facebook"></i></a>
         </div>
       </div>
    </div>

</footer>







<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="<?php echo base_url(); ?>assets/libs/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/slick/slick.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/handleCounter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/home.js"></script>

</body>
</html>