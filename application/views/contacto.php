<div class="container contacto">
	<img src="<?php echo base_url(); ?>assets/img/contacto.jpg" alt="">
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-6">
			<style>
				 #map {
			        height: 100%;
			      }
			      /* Optional: Makes the sample page fill the window. */
			      html, body {
			        height: 100%;
			        margin: 0;
			        padding: 0;
			      }
			</style>
			<div id="map">
				
			</div>
		</div>
		<div class="col-lg-6" style="border-top: solid 1px #000">
			<h1 class="font-weight-bold mb-5 pt-4">Lima</h1>
			<h4 class="font-weight-bold mb-3">Nuestra Oficina</h4>
			<p>Av. Salaverry 3320</p>
			<p>San Isidro, Lima</p>
			<strong>PERÚ</strong>

			<span class="font-weight-bold">(+51) 319-4300</span>



			<h4 class="font-weight-bold mt-5">Nuevos negocios, Marketing/PR o preguntas</h4>
			<p class="mb-5"><span class="font-weight-bold">Escríbenos a </span>hola@independencia.pro</p>
			

			<h4 class="font-weight-bold">Oportunidades de carrera</h4>
			<span>Envíanos tu <span class="font-weight-bold">CV y portafolio a</span></span>
			<p>carreras@independencia.pro</p>
			
		</div>
	</div>
	
	  <script>
			// Initialize and add the map
			function initMap() {
			  // The location of Uluru
			  var uluru = {lat: -12.100231, lng: -77.057310};
			  // The map, centered at Uluru
			  var map = new google.maps.Map(
			      document.getElementById('map'), {
			      	zoom: 16,
			      	center: uluru,
			      	styles: [
						    {
						        "featureType": "water",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#d3d3d3"
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "stylers": [
						            {
						                "color": "#808080"
						            },
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#b3b3b3"
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ffffff"
						            },
						            {
						                "weight": 1.8
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#d7d7d7"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ebebeb"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#a7a7a7"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#efefef"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#696969"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#737373"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "labels",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#d6d6d6"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {},
						    {
						        "featureType": "poi",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#dadada"
						            }
						        ]
						    }
						]
			      });
			  // The marker, positioned at Uluru
			  var marker = new google.maps.Marker({position: uluru, map: map});
			}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDi4uwTGeIH6TD9POaECMRmwMjO8Rr3SAw&callback=initMap"
    async defer></script>

</div>