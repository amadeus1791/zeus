<div class="container nosotros tope pr-5 pl-5">
    <h2>Manifiesto</h2>
    <p>
        Son 7 años y este año cumplimos un año más de vida. No vamos a parar hasta seguir demostrando que la publicidad
        es demasiado aburrida para ser tradicional. No vamos a parar hasta seguir creando mensajes que luchen por la
        equidad. No vamos a parar hasta demostrar que todos tienen derecho al éxito. No vamos a parar por darle
        humanidad a las marcas grandes y glamour a las marcas chicas. Seguiremos batallando para que la suma de nuestras
        pasiones genere una solución tan brillante que sorprenda a todos.
    </p>
    <p style="text-align: center">
        <img width="100%" src="<?php echo base_url(); ?>assets/img/image-nosotros.jpg" alt="">
    </p>

</div>

<div class="container mt-5">
    <h1 class="font-weight-bold">Equipo ejecutivo</h1>
    <div class="equipo">
        <div class="item-equipo">
            <img src="<?php echo base_url(); ?>assets/img/equipo/presidente.jpg" alt="">
            <h3>Armando Andrade</h3>
            <p>Presidente</p>
        </div>
        <div class="item-equipo">
            <img src="<?php echo base_url(); ?>assets/img/equipo/gerente.jpg" alt="">
            <h3>Jose Canonico</h3>
            <p>Gerente General</p>
        </div>
        <div class="item-equipo">
            <img src="<?php echo base_url(); ?>assets/img/equipo/director2.jpg" alt="">
            <h3>Marco Caballero</h3>
            <p>Director General Creativo</p>
        </div>
        <div class="item-equipo">
            <img src="<?php echo base_url(); ?>assets/img/equipo/director1.jpg" alt="">
            <h3>Germán Garrido</h3>
            <p>Director General Creativo</p>
        </div>
        <div class="item-equipo">
            <img src="<?php echo base_url(); ?>assets/img/equipo/planeamiento.jpg" alt="">
            <h3>Rafael Andrade</h3>
            <p>Director de Planeamiento</p>
        </div>
    </div>

    <h2 class="font-weight-bold">Talento Independiente</h2>

    <div class="equipo-completo">
        <div class="row">
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/1.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/2.jpg" alt="">
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/3.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/4.jpg" alt="">

                </div>
            </div>

            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/5.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/6.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/7.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/8.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/9.jpg" alt="">

                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/10.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/11.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/12.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/13.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/14.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/15.jpg" alt="">
                </div>

            </div>
            <div class="col-6 col-md-3">
                <div class="equipo-item">
                    <img src="<?php echo base_url(); ?>assets/img/equipo/team/16.jpg" alt="">
                </div>

            </div>

        </div>

    </div>
</div>

<div class="container mb-5">
    <div class="row">
        <div class="col-lg-4">
            <h2 class="font-weight-bold">Premios</h2>
            <h5>+500 premios en nuestra historia</h5>
            <p>27 premios en el 2016-2017</p>
        </div>
        <div class="col-lg-8">
            <img width="100%" src="<?php echo base_url(); ?>assets/img/equipo/premios.jpg" alt="">
        </div>
    </div>
</div>



















