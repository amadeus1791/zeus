<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="UTF-8">
	<title>Independencia</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!--<link rel="stylesheet" href="bootstrap/css/bootstrap-flaty.min.css">-->

  <script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/popper.min.js"></script>
  
  <script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
  <script>
  var path = '<?php echo base_url(); ?>';
  </script>
  
<style>
    .btn-logout{
      float: right;
    }
    </style>
</head>
<body>


 <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?php echo base_url(); ?>admin">Independencia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>admin/newsletter">Newsletter <span class="sr-only">(current)</span></a>
      </li>
      
    </ul>
    
  </div>
  <button class="btn btn-danger btn-logout"><i class="fas fa-sign-out-alt"></i> Logout</button>
</nav>
<br><br><br>
