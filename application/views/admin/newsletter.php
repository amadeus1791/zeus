<script src="<?php echo base_url(); ?>assets/admin/js/newsletter.js"></script>


<style>
    .estone{
        color: deepskyblue;
        font-size: 20px;
    }

    .estwo{
        color: #3dac4a;
        font-size: 20px;
    }
</style>


<div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">¡Alerta!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Está seguro que desea eliminar esta cita?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bt-eliminar btn-success" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="container" id="newsletter">




    <h1>Newsletter</h1>
    <br>

    <br>
    <div class="row">
        <button class="btn btn-info btn-descargar" style="margin-bottom: 20px"><i class="fas fa-download"></i> Descargar en CSV</button>
    </div>
    <div class="row justify-content-center">

        <nav aria-label="Page navigation example">
            <ul class="pagination">

            </ul>
        </nav>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody class="lista">

            </tbody>
        </table>

        <nav aria-label="Page navigation example">
            <ul class="pagination">

            </ul>
        </nav>

    </div>





</div>
