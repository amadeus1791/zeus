<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Independencia</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/home.css">
    <!--<link rel="stylesheet" href="bootstrap/css/bootstrap-flaty.min.css">-->

	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	
	<script>
	var path = '<?php echo base_url(); ?>';
	</script>
	<style>
        html, body{
            height: 100%;
        }
	form{
		width: 400px;
		margin: auto;
	}
	input{
		margin: 20px 0;
	}
	.btn-success{
		margin: auto;
		display: block;
		width: 100%;
	}
	.franja{
		background-color: #000;
		padding-top: 40px;
		padding-bottom: 20px;
	}
	.franja .logo{
		display: block;
		margin: auto;
	}
        img{
            height: 200px;
            width: auto;
        }
	</style>
</head>
<body>

<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-center" style="height: 100%;">
    <div class="row">
        <div class="col-12 col-lg-6">
            <img src="<?php echo base_url() ?>assets/img/logotipo.png" width="200" alt="" class="logo">
        </div>
        <div class="col-12 col-lg-6">
            <div id="login">


                <form class="form-signin" role="form" action="<?php echo base_url() ?>administrador/login" method="post">
                    <br>

                    <input type="text" class="form-control inuser" placeholder="Usuario" name="usuario" required autofocus>
                    <input type="password" class="form-control inclave" placeholder="Clave" name="clave" required>

                    <button class="btn btn-success" type="submit">Entrar</button>
                    <br><br>
                    <?php if(isset($_GET["error"])){ ?>
                        <div class="alert alert-danger">¡Usuario y/o clave incorrectos!</div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>



</body>
</html>