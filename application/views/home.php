<section class="banner">


    <div class="slick-home">
        <div class="no-back">
            <div class="row align-items-center">

                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img src="<?php echo base_url() ?>assets/img/botellas-banner.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row align-items-center">

                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img src="<?php echo base_url() ?>assets/img/b1.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row align-items-center">
                <div class="offset-lg-1 col-lg-5 col-md-6 order-2 order-md-1">
                    <img class="d-none d-md-block" src="<?php echo base_url() ?>assets/img/b2-v1.png" alt="">
                    <img class="d-block d-md-none" src="<?php echo base_url() ?>assets/img/b2-v3.png" alt="">
                </div>
                <div class="col-lg-6 col-md-6 order-1 order-md-2">
                    <div class="info-banner">
                        <strong>Mercaderismo & Posición</strong>
                        <p> Somos la solución para el posicionamiento de tus productos en el mercado.</p>
                        <a class="btn-zeus" href="#cotiza">¡HAGAMOS NEGOCIO!</a>
                    </div>

                </div>
            </div>
        </div>
    </div>


</section>

<section class="incrementar" id="incrementar">

    <div class="incrementar__left">
        <img src="<?php echo base_url() ?>assets/img/laptop.png" alt="" class="laptop">
    </div>
    <div class="incrementar__right">
        <h4>
            Empieza a Incrementar tus ventas
        </h4>
        <p>
            Ponemos tu producto al alcance del cliente, mejorando su visibilidad y disposición. Además, te brindamos
            herramientas digitales que faciliten tu gestión y seguimiento a tiempo real.
        </p>
        <div class="numbers">
            <div class="numbers__item">
                <span>+25%</span>
                <p>ventas</p>
            </div>
            <div class="numbers__item">
                <span>+40%</span>
                <p>Alcance</p>
            </div>
            <div class="numbers__item">
                <span>-30%</span>
                <p>Costo</p>
            </div>
        </div>
    </div>
</section>

<section class="cotizador" id="cotiza">
    <div class="zeus-title">
        Un servicio a tu alcance
    </div>
    <p class="zeus-subtitle">
        Ofrecemos el servicio más flexible. Calcula el costo de nuestro servicio de acuerdo a tu necesidad.
    </p>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">

                <div class="form-group">
                    <label for="">¿Dónde ventes tus productos?</label>

                    <div class="block-select">
                        <img src="<?php echo base_url() ?>assets/img/arrow-down.svg" alt="">
                        <select name="donde" id="">
                            <option value="supermercado">Supermercado</option>
                            <option value="tienda">Tienda</option>

                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <label for="">¿Cuántos son tus puntos de venta?</label>
                    <div class="handle-counter" id="venta">
                        <button class="counter-minus">-</button>
                        <input type="text" name="punto" value="1" pattern="\d*"/>
                        <button class="counter-plus">+</button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">¿Cuántos productos ofreces (SKU)?</label>
                    <div class="handle-counter" id="tipo">
                        <button class="counter-minus">-</button>
                        <input type="text" name="sku" value="1" pattern="\d*"/>
                        <button class="counter-plus">+</button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Selecciona la cantidad visitas:</label>

                    <div class="block-select">
                        <img src="<?php echo base_url() ?>assets/img/arrow-down.svg" alt="">
                        <select name="visitas" id="">
                            <option value="4">1 visita por semana (Recomendado)</option>
                            <option value="8">2 visitas por semana</option>
                            <option value="12">3 visitas por semana</option>
                            <option value="16">4 visitas por semana</option>
                            <option value="20">5 visitas por semana</option>
                            <option value="24">6 visitas por semana</option>
                            <option value="28">7 visitas por semana</option>
                            <option value="1">1 visita al Mes</option>
                            <option value="2">2 visitas al Mes</option>
                            <option value="3">3 visitas al Mes</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-6">
                <div class="result">
                    <span>Tu pago mensual sería</span>

                    <div class="price">
                        <i>s/</i>
                        <div class="num" id="num">
                            <strong>00</strong> <span>*</span>
                            <input type="hidden" name="coti"/>
                        </div>
                        <p>Mercaderismo + seguimiento virtual</p>
                        <strong>*No incluye IGV</strong>
                    </div>
                    <button class="btn-zeus" data-toggle="modal" data-target="#cotizacion">¡COTIZA AQUÍ!</button>
                </div>

                <div class="duda">¿Tienes dudas? Escríbenos</div>
            </div>
        </div>
    </div>
</section>


<section class="diferencia" id="beneficios">
    <div class="container">
        <div class="zeus-title">Un servicio integral</div>
        <p class="zeus-subtitle">
            Con Zeus podrás tener mayor control y visibilidad de la exposición de tus productos</p>

        <div class="row diferencia-carousel">
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/clock.svg" alt="">
                    <div>
                        <strong>Información real y precisa</strong>
                        <p>
                            Monitoreamos la cantidad y posición de tus productos en góndola, almacén y fechas de
                            vencimiento para brindarte información a tiempo real.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/stock.svg" alt="">
                    <div>
                        <strong>Niveles de Stock Optimizados</strong>
                        <p>
                            Optimizamos los niveles de stock en base a tu prioridad de ventas para que pedas tener más
                            cantidad de productos en góndola y menos en almacén.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/eficientes.svg" alt="">
                    <div>
                        <strong>Reposiciones Eficientes en Tienda</strong>
                        <p>
                            Nos encargamos de tus productos desde la salida de tu almacén hasta la entrega. Con nuestra
                            opción de envíos (moto, auto o camión) automatizamos tus entregas para que sean más
                            puntuales y eficientes.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="item-diferencia">
                    <img src="<?php echo base_url() ?>assets/img/preventivo.svg" alt="">
                    <div>
                        <strong>Control preventivo de Perdidas</strong>
                        <p>
                            En nuestros reportes periódicos , te informamos acerca del estado y vencimiento de tus
                            productos en stock para que puedas tomar medidas preventivas y oportunas.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="clientes">
    <div class="container">
        <div class="zeus-title">Nuestra Experiencia</div>
        <p class="zeus-subtitle">Trabajamos para hacer del mercaderismo un servicio más eficiente y sostenible </p>



        <div class="wraper-clientes">

            <div class="experiencia">
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/productos.svg" alt="">
                    <strong>+800.000</strong>
                    <p>Productos posicionados en el punto de venta</p>
                </div>
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/location.svg" alt="">
                    <strong>+500</strong>
                    <p>Puntos de venta visitados mensualmente</p>
                </div>
                <div class="experiencia__item">
                    <img src="<?php echo base_url() ?>assets/img/time.svg" alt="">
                    <strong>+20,000</strong>
                    <p>Horas de ejecución impecable al año</p>
                </div>
            </div>

            <div class="testimonios">
                <div class="testimonio show" id="nutrishake">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/nutrishake-back.jpg)">

                    </div>
                    <div class="test-info">
                        <p>“Gracias a los servicios de Zeus logramos mejorar nuestras ventas en un 20% y reducir nuestros
                            costos en 25%. Además de reducir el tiempo de 4 a 1 día para abastecer y posicionar nuestros
                            productos en las tiendas de autoservicios(Tambo 200 y Listo 100).”</p>

                        <span>Daniel Nuñez</span>
                        <span>CEO & Founder </span>
                        <strong>Nutrishake</strong>
                    </div>
                </div>
                <div class="testimonio" id="vidria">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/vidria-back.jpg)">
                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo en supermercados de Zeus, la frecuencia de pedidos nuevos paso
                            de 1 a 2 por mes. Además que tenemos más espacio en góndola.”</p>

                        <span>Marco Riva</span>
                        <span>CEO</span>
                        <strong>Vidria</strong>
                    </div>
                </div>
                <div class="testimonio" id="ecohouse">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/ecohouse-back.jpg)">

                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo de Zeus aumentamos nuestra presencia en góndola en un 30% lo que
                            generó un aumento en ventas del 15%.”</p>

                        <span>Gina Scamarone</span>
                        <span>Gerente Comercial</span>
                        <strong>Ecohouse</strong>
                    </div>
                </div>
                <div class="testimonio" id="prots">
                    <div class="test-pic" style="background-image: url(<?php echo base_url() ?>assets/img/prots-back.jpg)">
                    </div>
                    <div class="test-info">
                        <p>“Gracias al servicio de mercaderismo de Zeus en Tottus, tenemos un mejor monitoreo de nuestros
                            productos por vencer en cada tienda, lo cual nos permite tomar medidas correctivas y reducir nuestra
                            merma en un 30%.”</p>

                        <span>Billy Jimenez</span>
                        <span>Jefe de Producción</span>
                        <strong>Prots</strong>
                    </div>
                </div>

            </div>

            <div class="row buttons-clientes">
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item active" data-cliente="nutrishake">
                        <img src="<?php echo base_url() ?>assets/img/nutrishake.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="vidria">
                        <img src="<?php echo base_url() ?>assets/img/vidria-logo.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="ecohouse">
                        <img src="<?php echo base_url() ?>assets/img/house.svg" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cliente-item" data-cliente="prots">
                        <img src="<?php echo base_url() ?>assets/img/prots.svg" alt="">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

