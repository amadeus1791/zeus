<!doctype html>
<html lang="es">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/slick/slick.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/slick/slick-theme.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/owl-carousel/owl.carousel.min.css">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">

  <script>
    var path = '<?php echo base_url(); ?>';
  </script>
  <title>Zeus.pe | <?php echo $titulo; ?></title>
  
</head>
<body>

<div class="modal fade" id="cotizacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="container-fluid">
                <div class="row justify-content-end mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6 d-none d-lg-block">
                        <img width="100%" src="<?php echo base_url() ;?>assets/img/undraw_business_deal_cpi9.svg" alt="">
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <form action="<?php echo base_url()?>contacto/enviar" method="post">
                            <input type="hidden" name="dondeCoti">
                            <input type="hidden" name="ptosVenta">
                            <input type="hidden" name="cantidad">
                            <input type="hidden" name="visitas">
                            <input type="hidden" name="cotizacion">

                            <div class="form-group">
                                <label for=""><i class="fas fa-user"></i>Nombre y Apellido</label>
                                <input type="text" name="nombres" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-envelope"></i>Correo Electrónico</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-newspaper"></i>RUC</label>
                                <input type="number" name="ruc" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fas fa-phone"></i>Teléfono</label>
                                <input type="text" name="telefono" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-zeus">¡Hagamos un negocio!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="enviado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="container-fluid">

                <div class="row text-center">
                    <div class="col-lg-8 offset-lg-2">
                        <img width="100%" src="<?php echo base_url() ?>assets/img/enviado.svg" alt="">
                    </div>
                    <div class="col-lg-12 text-center">
                        <strong>Enviado correctamente</strong>
                        <button class="btn-zeus" data-dismiss="modal" aria-label="Close">¡Aceptar!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<nav class="menu">
    <div class="container navbar">
        <a href="<?php echo base_url(); ?>" class="menu__brand">
          <img src="<?php echo base_url() ?>assets/img/logo.svg" alt="">
        </a>
        <div class="menu__button">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="close__button">
          <i class="fas fa-times"></i>
        </div>
        <ul class="menu__box">
            <li class="menu__item logo-resp">
                <img src="<?php echo base_url() ?>assets/img/logo-blanco.svg" alt="">
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#cotiza">Cotiza</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#mercaderismo">Mercaderismo</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#beneficios">Beneficios</a>
            </li>
            <li class="menu__item">
              <a class="menu__link" href="#" data-toggle="modal" data-target="#cotizacion">Contacto</a>
            </li>
            <li class="menu__item redes">
                <a class="menu__link" href="#"><span>Síguenos en:</span> <i class="fab fa-facebook"></i></a>
            </li>
        </ul>
    </div>
</nav>