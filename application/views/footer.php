<footer>
    <div class="container">
       <div class="row">
           <div class="col-lg-3">
               <img width="150px" src="<?php echo base_url()?>assets/img/logo-blanco.svg" alt="">
           </div>
           <div class="col-lg-6">
               <strong>¿Quiéres saber más?</strong>
               <p>Te invitamos a contactarnos a través de los siguientes canales de comunicación, te atenderemos en la brevedad posible.</p>
           </div>
           <div class="col-lg-3">
              <div class="sociales">
                  <a href=""><i class="fas fa-envelope"></i></a>
                  <a href=""><i class="fab fa-whatsapp"></i></a>
                  <a href=""><i class="fab fa-facebook-messenger"></i></a>
              </div>
           </div>
       </div>
       <div class="row copy">
         <div class="col-lg-6 offset-lg-3 text-center">
           © Copyright 2012 - 2019   |   All Rights Reserved  
         </div>
         <div class="col-lg-3 text-md-right">
           Síguenos en <a href=""><i class="fab fa-facebook"></i></a>
         </div>
       </div>
    </div>

</footer>







<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="<?php echo base_url(); ?>assets/libs/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/slick/slick.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/handleCounter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/home.js"></script>

</body>
</html>