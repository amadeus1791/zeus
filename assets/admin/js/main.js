$(document).ready(function(){
    

	$(".btn-logout").click(function(){
		location.href=path+"administrador/logout";
	});
})


var Request = function(ac,params,callback,method){

	var m = "get";

	if(method!=undefined) m = method;

    $.ajax({
        url:path+ac,
        dataType:"json",
        data:params,
        type:m,
        success:callback
    });
}


var ExportCSV = function(_data){

    var objectdata  = _data;

    this.convertArrayOfObjectsToCSV = function(args) {  
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ';';
        lineDelimiter = args.lineDelimiter || '\n';

        keys = Object.keys(data[0]);

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(function(item) {
            ctr = 0;

            keys.forEach(function(key) {

                if (ctr > 0) result += columnDelimiter;
                result += item[key];

                ctr++;
            });
            result += lineDelimiter;
            
        });

        return result;
    }

    this.downloadCSV = function(args) {  //downloadCSV({ filename: "stock-data.csv" });
        var data, filename, link;
        var csv = this.convertArrayOfObjectsToCSV({
            data: objectdata
        });
        if (csv == null) return;

        filename = args.filename || 'export.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=iso-8859-15,' + csv;
        }
        data = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
    }

}

var Alerta = function(tit,msg){
    $("#modalalerta .modal-title").html(tit);
    $("#modalalerta .modal-body").html(msg);
    $("#modalalerta").modal("show");
}