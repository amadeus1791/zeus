var pagina = 1;
var numero_pag = 0;

$(document).ready(function(){
    obtenertotal();
    listar();

});




function obtenertotal(){
    new Request("newsletter/total/",null,function(res){
        //console.log(res);
        var total = parseInt(res.total);
        numero_pag = Math.ceil(total / 50);
        $('.pagination ').append('<li class="page-item2"><a class="page-link" href="javascript: anterior()">Anterior</a></li>');



        for ( i=0; i< numero_pag ; i++ ){

            var html = '<li class="page-item"><a class="page-link" href="javascript: paginar('+ (i+1) +')">'+ (i+1) +'</a></li>';
            $('.pagination ').append(html);

            $(".page-item").click(function () {
                if (i == numero_pag) {
                    $(".page-item").removeClass("active");
                    $(this).addClass("active");
                }
            });
        }

        $('.pagination ').append('<li class="page-item2"><a class="page-link" href="javascript: siguiente()">Siguiente</a></li>');

    });

}

function anterior() {

    if( pagina > 1){
        pagina--;
        listar();
        $(".page-item").removeClass("active");

    }


}



function siguiente() {

    if( pagina < numero_pag ){
        pagina++;

        listar();
        $(".page-item").removeClass("active");


    }

}

function paginar(num) {


    pagina = num;

    listar();



}






function listar(){



    $("#newsletter .lista").empty();

    new Request("newsletter/listar/",{pagina:pagina},function(res){

        $("#newsletter .lista").empty();
        $.each(res,function(k,v){
            var it = new ItemEmail(v);
            $("#newsletter .lista").append(it);
        })
    });

    $(".btn-descargar").click(function(){
        $(".btn-descargar").attr("disabled",true);
        $(".btn-descargar").html("Descargando...");
        new Request("newsletter/descargaremail/0",null,function(data){
            var ecsv = new ExportCSV(data);
            ecsv.downloadCSV({ filename: "descargaemail.csv" });
            $(".btn-descargar").attr("disabled",false);
            $(".btn-descargar").html("Descargar en CSV");
        });


        var ExportCSV = function(_data){

            var objectdata  = _data;

            this.convertArrayOfObjectsToCSV = function(args) {
                var result, ctr, keys, columnDelimiter, lineDelimiter, data;

                data = args.data || null;
                if (data == null || !data.length) {
                    return null;
                }

                columnDelimiter = args.columnDelimiter || ';';
                lineDelimiter = args.lineDelimiter || '\n';

                keys = Object.keys(data[0]);

                result = '';
                result += keys.join(columnDelimiter);
                result += lineDelimiter;

                data.forEach(function(item) {
                    ctr = 0;

                    keys.forEach(function(key) {

                        if (ctr > 0) result += columnDelimiter;
                        result += item[key];

                        ctr++;
                    });
                    result += lineDelimiter;

                });

                return result;
            };

            this.downloadCSV = function(args) {  //downloadCSV({ filename: "stock-data.csv" });
                var data, filename, link;
                var csv = this.convertArrayOfObjectsToCSV({
                    data: objectdata
                });
                if (csv == null) return;

                filename = args.filename || 'export.csv';

                if (!csv.match(/^data:text\/csv/i)) {
                    csv = 'data:text/csv;charset=iso-8859-15,' + csv;
                }
                data = encodeURI(csv);

                link = document.createElement('a');
                link.setAttribute('href', data);
                link.setAttribute('download', filename);
                link.click();
            }

        }

    });

}


var ItemEmail = function(data){


    var html = $('<tr width="100%" id="news_'+data.news_id+'">'+
        '<td>'+data.news_id+'</td>'+
        '<td>'+data.news_email+'</td>'+
        '</tr>');



    return html;



};




