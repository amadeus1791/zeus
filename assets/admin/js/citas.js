var meses = Array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
$(document).ready(function(){

	listar();

	


	

});


function listar(){
	$("#citas .lista").empty();

	new Request("cita/listar/",null,function(res){
		console.log(res);
		$("#citas .lista").empty();
		$.each(res,function(k,v){
			var it = new ItemCita(v);
			$("#citas .lista").append(it);
		})

	
	});

	
}


var ItemCita = function(data){
	var nom = data.cta_nombres+' '+data.cta_apepat+' '+data.cta_apemat;
	var horario = '';

	if(data.cta_horario<12){
		horario = data.cta_horario+':00 A.M.';
	}else if(data.cta_horario >12){
		horario = (parseInt(data.cta_horario) - 12).toString()+':00&nbsp;P.M.';
	}else{
		horario = data.cta_horario+':00 P.M.';
	}

	var fecha = data.cta_fecha.split("-");
	var m = parseInt(fecha[1]);
	var f = fecha[2]+" "+meses[m - 1];



	var html = $('<tr width="100%" id="cta_'+data.cta_id+'">'+
					'<td>'+f+'</td>'+
					'<td>'+horario+'</td>'+
					'<td>'+nom+'</td>'+
					'<td>'+data.cta_email+'</td>'+
					'<td>'+data.cta_distrito+'</td>'+
					'<td>'+data.cta_celular+'</td>'+

					
					'<td><div class="btn-group" role="group" aria-label="...">'+
  			'<button type="button" class="btn btn-outline-danger btn-eliminar"><i class="fas fa-minus-circle"></i></button>'+
			'</div></td>'+
				'</tr>');
	

	html.find(".btn-eliminar").click(function(){

		$("#modaleliminar").modal("show");

		$("#modaleliminar .bt-eliminar").unbind();
		$("#modaleliminar .bt-eliminar").click(function(){
			$("#modaleliminar").modal("hide");
			$("#cta_"+data.cta_id).remove();
				
			new Request("cita/eliminar/"+data.cta_id,null,function(res){
				console.log(res);
				
			});
		

			
		})

	});
	return html;
	

};


$(document).ready(function () {
    $('#entradafilter').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.lista tr').hide();
        $('.lista tr').filter(function () {
            return rex.test($(this).text());
        }).show();

    })

});